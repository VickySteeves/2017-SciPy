#  Open Data & Reproducibility | SciPy 2017
## Creating Reproducible Experiments with ReproZip
### Schedule
Thursday July 13th, 15:30-16h, Room 204

### Short Abstract
Reproducibility is a core component of the scientific process: it helps researchers all around the world to verify research results and also to build on them, allowing science to move forward. Unfortunately, computational reproducibility can be very painful. We’ll present an open source tool for computational reproducibility, [ReproZip](https://reprozip.org). ReproZip is written in Python, and was designed to simplify the process of making an experiment reproducible across platforms. ReproZip creates self-contained, reproducible packages by automatically tracking, identifying, and capturing all its required dependencies: programs, libraries, data, and configuration files. The original user can share the package with others, who can then use ReproZip to unpack and rerun the experiment on their favorite operating system.

### Long Abstract 
Reproducibility is a core component of the scientific process: it helps researchers all around the world to verify research results and also to build on them, allowing science to move forward. Unfortunately, computational reproducibility can be very painful to achieve for a number of reasons. Consider the author-reviewer scenario of a scientific paper: authors must generate a compendium that captures all the dependencies needed to reproduce their experiments, including a complete specification of the experiment and information about the originating computational environment. Keeping track of this information manually is rarely feasible: it is both time-consuming and error-prone. First, computational environments are complex, consisting of many layers of hardware and software, and the configuration of the OS is often hidden. Second, tracking library dependencies is challenging, especially for large experiments.
 
This talk will present an open source tool for computational reproducibility of science, [ReproZip](https://reprozip.org). ReproZip is written in Python, and was designed to simplify the process of making an existing script or experiment reproducible across platforms, even if the researcher didn't think about reproducibility at the start of the project. ReproZip creates self-contained, reproducible packages by automatically tracking, identifying, and capturing all its required dependencies: programs, software libraries, data, and configuration files. The original user can share the package with others, who can then use ReproZip to unpack it, and rerun the experiment on their favorite operating system.
 
Currently, users can "pack" their work using ReproZip on Linux operating systems via the command line. This is accomplished by prepending the command 'reprozip trace' to the execution of their research, e.g. 'reprozip trace python analysis.py'. ReproZip collects information including command-line arguments, environment variables, files read, and files written, and stores everything in a SQLite database. Then, users generate a ReproZip package using the command 'reprozip pack <package-name> ' which compresses all the dependencies, files, and environmental information into a small .rpz file.
 
ReproZip allows users to unpack a .rpz package on any operating system, using the command line or the GUI. Currently, there are four unpackers: directory, chroot, Vagrant, and Docker. If using the last two (for instance, in reproducing research across different operating systems) ReproZip will automatically create and start the virtual machine or Docker container.  ReproZip was designed to allow anyone to create new plugins to unpack .rpz files, which will support every package created in the past.  Other users can also upload their own data and configuration files into the unpacked environment, without having to track down and install the requirements on their machine.
 
ReproZip has users and [use cases](https://examples.reprozip.org) across scientific domains and methods, from visualization to simulations. We will demo an example of packing and unpacking a Jupyter Notebook of neuroscience research for reproducibility and a machine learning example using the scikit-learn library. We will also discuss how ReproZip can be used in distributed environments and its limitations.





